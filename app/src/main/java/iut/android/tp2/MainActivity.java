package iut.android.tp2;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        EditText messageEditor = findViewById(R.id.messageEditor);

        Button buttonB = findViewById(R.id.startActivityB);
        Button buttonC = findViewById(R.id.startActivityC);
        //Listener for sending to the B activity
        View.OnClickListener goToB = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, ActivityB.class);
                intent.putExtra("KEY_FROM_A",messageEditor.getText().toString());
                startActivityForResult(intent,1);
            }
        };
        //Listener for sending to the C activity
        View.OnClickListener goToC = new View.OnClickListener(){
            @Override
            public void onClick(View view){
                Intent intent = new Intent(MainActivity.this, ActivityC.class);
                intent.putExtra("KEY_FROM_A",messageEditor.getText().toString());
                startActivityForResult(intent,2);
            }
        };

        buttonB.setOnClickListener(goToB);
        buttonC.setOnClickListener(goToC);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == 1){
            TextView activityFrom = findViewById(R.id.ActivityFrom);
            activityFrom.setText("Activity B");
            TextView callbackMessage = findViewById(R.id.callbackMessage);
            callbackMessage.setText(data.getStringExtra("KEY_FROM_B"));
        } else if (requestCode == 2){
            TextView activityFrom = findViewById(R.id.ActivityFrom);
            activityFrom.setText("Activity C");
            TextView callbackMessage = findViewById(R.id.callbackMessage);
            callbackMessage.setText(data.getStringExtra("KEY_FROM_C"));
        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        EditText messageEditor = findViewById(R.id.messageEditor);
        messageEditor.setText("");
    }
}