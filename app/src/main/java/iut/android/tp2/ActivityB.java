package iut.android.tp2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

public class ActivityB extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_b);

        String FROM_ACTIVITY_A = getIntent().getStringExtra("KEY_FROM_A");
        TextView middleTextView = findViewById(R.id.middleTextView);
        middleTextView.setText(FROM_ACTIVITY_A);
    }

    @Override
    public void onBackPressed() {
        TextView callbackMessage = findViewById(R.id.callbackMessage);
        Intent intent = new Intent();
        intent.putExtra("KEY_FROM_B", callbackMessage.getText().toString());
        setResult(RESULT_OK,intent);
        super.onBackPressed();
    }
}